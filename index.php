<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<?php include('data.php') ?>
<link rel="stylesheet" type="text/css" media="all" href="css/main.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
	var phpArrayGiven = <?php echo json_encode($array); ?>;
</script>
<script type="text/javascript" src="js/forecasting.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="js/goodGraph.js"></script>
</head>
<body>
	<div class="header">
		<a href="additems.html"><img src="images/additems.png" style="float:left; width:428px; height:181px; margin-left:252px;" /></a>
		<a href="edititems.html"><img src="images/edititems.png" style="float:left;" /></a>
		<a href="./"><img src="images/forecasterselected.png" style="float:left;"/></a>
		<div style="display: block; clear: both;"></div>
	</div>
<div style="width:100%; height:20px;"></div>
<div style="width:100%; text-align:center;">
<br>
<h1 style="margin: 0 auto; margin-top:20px;">Monthly Data</h1>
<br>	
<form method="post" action="JavaScript:getMonthlySelection();">

<input type="hidden" name="submitted" value="true" />

		<label>Select month: 
 		<select size="1" name="selectedMonth" id="selectedMonth">
			<option value="1">January</option>
      			<option value="2">February</option>
      			<option value="3">March</option>
      			<option value="4">April</option>
      			<option value="5">May</option>
      			<option value="6">June</option>
      			<option value="7">July</option>
      			<option value="8">August</option>
      			<option value="9">September</option>
      			<option value="10">October</option>
      			<option value="11">November</option>
      			<option value="12">December</option>
		</select>
    </label>
   <label>Select Type of Food: 
 		<select size="1" name="selectedYear" id="selectedTypeforMonth">
			<option value="Tomatoes">Tomatoes</option>
      			<option value="Potatoes">Potatoes</option>
      			<option value="Peantur Butter">Peanut Butter</option>
		</select>
    </label>
    <input type="submit" id="GetMontlySelectionButton" value="Show Data" />
</form>
</div>

	<div id="containerT" style="min-width:1536px; heignt:1900px; margin: 0 auto;"></div>

<div style="width:100%; text-align:center;">
<br>
<h1 style="margin: 0 auto; margin-top:20px;">Yearly Data</h1>
<br>

<form method="post" action="JavaScript:getYearlySelection();">
	
                <label>Select year:
                <select size="1" name="selectedYear" id="selectedYear">
                        <option value="11">2011</option>
                        <option value="12">2012</option>
                </select>
    </label>
   <label>Select Type of Food: 
                <select size="1" name="selectedYear" id="selectedTypeforYear">
                        <option value="Tomatoes">Tomatoes</option>
                        <option value="Potatoes">Potatoes</option>
                        <option value="Peantur Butter">Peanut Butter</option>
        </select>
    </label>
    <input type="submit" id="getYearlySelectionButton" value="Show Data" />

</form>
</div>
	<div id="containerP" style="min-width:1536px; heignt:1900px; margin: 0 auto;"></div>


<div style="width:100%; text-align:center;">
<br>
<h1 style="margin: 0 auto; margin-top:20px;">Monthly Data Projection</h1>
<br>
<form method="post" action="JavaScript:getMonthlyProjectionSelection();">

<input type="hidden" name="submitted" value="true" />

                <label>Select month:
                <select size="1" name="selectedMonth" id="selectedMonthP">
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                </select>
    </label>
   <label>Select Type of Food:
                <select size="1" name="selectedYear" id="selectedTypeforMonthP">
                        <option value="Tomatoes">Tomatoes</option>
                        <option value="Potatoes">Potatoes</option>
                        <option value="Peantur Butter">Peanut Butter</option>
                </select>
    </label>
    <input type="submit" id="GetMonthlyProjectionSelectionButton" value="Show Data" />
</form>
</div>

        <div id="containerB" style="min-width:1536px; heignt:1900px; margin: 0 auto;"></div>

	<script type="text/javascript">
        	window.onload = function(){ 	graphDay('containerT', getMonthlyData(11,12,"Tomatoes") , 'Tomatoes');
                				graphMonth('containerP', getYearlyData(12,"Tomatoes") , 'Tomatoes');
						graphDay('containerB', getProjectedMonth(5,12,"Tomatoes")[0] , 'Tomatoes');
                			}; 
	 	function getMonthlySelection() {
			var month = document.getElementById('selectedMonth');
			var monthNum = month.options[month.selectedIndex].value;
			monthNum = parseInt(monthNum);
			var product = document.getElementById('selectedTypeforMonth');
                        var productType = product.options[product.selectedIndex].text;
			graphDay('containerT', getMonthlyData(monthNum,12,productType) , productType);
		}
		function getYearlySelection() {
                        var year = document.getElementById('selectedYear');
                        var yearNum = year.options[year.selectedIndex].value;
                        yearNum = parseInt(yearNum);
                        var product = document.getElementById('selectedTypeforYear');
                        var productType = product.options[product.selectedIndex].text;
                        graphMonth('containerP', getYearlyData(yearNum,productType) , productType);
                }
		function getMonthlyProjectionSelection() {
                        var month = document.getElementById('selectedMonthP');
                        var monthNum = month.options[month.selectedIndex].value;
                        monthNum = parseInt(monthNum);
                        var product = document.getElementById('selectedTypeforMonthP');
                        var productType = product.options[product.selectedIndex].text;
                        graphDay('containerB', getProjectedMonth(monthNum,12,productType)[0] , productType);
                }
	</script>
</body>
</html>
